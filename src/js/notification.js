// your code here
window.Notifications ={
	notify: function(message, time, type){
		var messageBox = $('<div></div>').addClass('box');
		if(!$('body').find('.box').length){
			$(messageBox).appendTo('body');
		}
		var className= type ? type + ' message' : 'message';
		var msg = message ? message : 'Default message';
		var time = time ? time*1000 : 3000; //convert to miliseconds
		if(time > 36000){   // check for time being too long, lessen the time
			time = 10000;
		}
		var removeClass ='';
		for (var i = className.split(' ').length - 1; i >= 0; i--) {
			removeClass='.'+className.split(' ')[i];
			};
		var notificationBox = $('<div></div>').addClass(className).html(msg);
		$(notificationBox).appendTo('.box').hide().fadeIn(500);
		if(time && isFinite(time) && removeClass!=='.message'){
		 setTimeout(function(){
 			$(removeClass).fadeOut(300);
		 }, time);
		}
	}
}